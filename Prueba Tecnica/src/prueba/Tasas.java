// Clase donde acumularemos las distintas tasas de cada tarjeta (tipo auxiliar, solo sirve para consultar las tasas)

package prueba;

public class Tasas {
 
	// Metodo para obtener la tasa de interes de la compra con tarjeta Visa
	public Double getTasaVisa(Integer mes, Integer anio, Double consumo) {
		
		consumo = consumo + (consumo * (mes*anio)/100);
		
		return consumo;
	}

	// Metodo para obtener la tasa de interes de la compra con tarjeta Naranja
	public Double getTasaNara(Integer mes, Double consumo) {

		consumo = consumo + (consumo * (mes*0.5)/100);
		return consumo;
	}

	// Metodo para obtener la tasa de interes de la compra con tarjeta American
	// Expres
	public Double getTasaAmex(Integer mes, Double consumo) {

		consumo = consumo + (consumo *((mes*0.1)));
		return consumo;
	}
}
