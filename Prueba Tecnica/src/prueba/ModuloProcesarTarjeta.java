package prueba;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import tarjetas.Tarjeta;

public class ModuloProcesarTarjeta {

	private Tarjeta tarjetaProcesar;
	private Double consumo;

	// Constructor
	public ModuloProcesarTarjeta(Tarjeta tarjeta, Double monto) {

		this.tarjetaProcesar = tarjeta;

		this.consumo = monto;

	}

	// Metodos

	// Vemos si es posible realizar la operacion a partir del monto a cobrar

	private boolean operacionValida() {

		if (consumo < 1000 && consumo > 0) {

			return true;
		} else if (consumo <= 0) {

			throw new RuntimeException("El valor de consumo: " + consumo + " no puede ser menor o igual a 0");
		} else {

			return false;
		}
	}

	public boolean GetOperacionValida() {
		return operacionValida();
	}

	// Obtenemos el a�o actual en formato String

	private String obtenerAnio() {

		String anio = new SimpleDateFormat("yy").format(Calendar.getInstance().getTime());

		return anio;
	}

	// Obtenemos el mes actual en formato String

	private String obtenerMes() {

		String mes = new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());

		return mes;

	}

	// Obtenemos el anio actual en formato Integer
	public Integer obtenerAnioInt() {

		Integer anio = 0;

		try {
			anio = Integer.parseInt(obtenerAnio());
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}

		return anio;
	}

	// Obtenemos el mes actual en formato Integer
	public Integer obtenerMesInt() {

		Integer mes = 0;

		try {
			mes = Integer.parseInt(obtenerMes());
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}

		return mes;
	}

	// Vemos si es posible cobrar con la tarjeta a partir de su vencimiento,
	// comparamos el mes/a�o de la tarjeta con el del actual

	public boolean GetTarjetaValidaParaOperar() {

		return comparar(tarjetaProcesar.getFechaVencimiento(), obtenerAnio(), obtenerMes());
	}

	// Compaamos a treves de expresiones regulares

	private boolean comparar(String fechaTarjeta, String anio, String mes) {

		// Comparamos usando Pattern y Matcher
		String mesAnioActual = (mes + "/" + anio);

		if (mesAnioActual.compareTo(fechaTarjeta) < 1) {

			return true;
		} else {

			return false;
		}
	}

	public Double GetTasaDeInteres() {

		return calcularTasaInteres();
	}

	// A partir de la marca de la tarjeta llamamos al metodo calcular interes
	// verificamos la marca y obtenemos la tasa de interes
	private Double calcularTasaInteres() {

		Tasas tasa = new Tasas();

		Double tasaCalculada = 0.0;

		String marca = tarjetaProcesar.getMarca();

		switch (marca) {
		case "Visa": {

			tasaCalculada = tasa.getTasaVisa(obtenerMesInt(), obtenerAnioInt(), consumo);
			return tasaCalculada;
		}
		case "Nara": {

			tasaCalculada = tasa.getTasaNara(obtenerMesInt(), consumo);
			return tasaCalculada;
		}
		case "Amex": {

			tasaCalculada = tasa.getTasaAmex(obtenerMesInt(), consumo);
			return tasaCalculada;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + tarjetaProcesar.getMarca());
		}

	}

}
