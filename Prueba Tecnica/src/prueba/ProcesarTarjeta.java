package prueba;

import tarjetas.Tarjeta;

public class ProcesarTarjeta {

	private Tarjeta tarjetaCliente;

	private ModuloProcesarTarjeta procesar;

	private MetodosAuxconTarjetas aux;

	private Double MontoACobrar;

	private String jsonTarjeta;

	// Construcor
	public ProcesarTarjeta(String JsonTarjeta, Double monto) {

		this.MontoACobrar = monto;

		this.jsonTarjeta = JsonTarjeta;

		aux = new MetodosAuxconTarjetas();

	}

	// Metodos

	// Vamos a recojer los datos de la tarjeta del cliente
	// Simulamos la entrada a traves de un json y obtenemos la tarjeta

	public void crearTarjetaCliente() {

		tarjetaCliente = aux.GetDeTarjetasGson(jsonTarjeta);
	}

	// Consultas al modulo

	// Creamos una nueva consulta al modulo pasando la tarjeta del cliente y el
	// monto que se desea procesar
	public void inicializarModulo() {

		procesar = new ModuloProcesarTarjeta(tarjetaCliente, MontoACobrar);
	}

	// Consultamos si la operacion es valida, usamos exepciones en caso que el valor
	// no corresponda a uno aceptable

	public void consultarOperacionValida() {

		try {
			procesar.GetOperacionValida();
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	// Consultamos si la tarjeta es valida para operar

	public void consultarTarjetaValida() {

		procesar.GetTarjetaValidaParaOperar();
	}

	// Obtenemos la tasa de interes a partir de la marca de la tarjeta

	public void consultarTasaDeInteres() {

		procesar.GetTasaDeInteres();
	}

	// Verificamos si las tarjetas son iguales, lo hacemos mediante el numero de la
	// misma
	public boolean compararTarjetas(Tarjeta cliente1, Tarjeta cliente2) {

		return aux.compararTarjetas(cliente1, cliente2);
	}

}
