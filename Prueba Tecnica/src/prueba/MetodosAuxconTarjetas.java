package prueba;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import tarjetas.Tarjeta;

public class MetodosAuxconTarjetas {


	// Metodo para crear tarjeta a partir de un Json
	// Lamentablemente no logre hacer que funcione

	public Tarjeta GetDeTarjetasGson(String jsontarjeta) {

		Tarjeta TarjetaLeida = null;

		String json = "";
 
		try {
			BufferedReader br = new BufferedReader(new FileReader(jsontarjeta));

			String linea = "";

			while ((linea = br.readLine()) != null) {

				json += linea;

			}
			br.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		Gson gson = new Gson();

		TarjetaLeida = gson.fromJson(json, Tarjeta.class);

		return TarjetaLeida;

	}

	// Vamos a guardar la informacion de una tarjeta en un Json para enviar
	public void SetInfoTarjetaGson(Tarjeta tarjeta) {

		Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();

		String representacion = prettyGson.toJson(tarjeta);

		System.out.println(representacion);

	}

	// Comparamos si dos tarjetas son iguales a partir de su numero
	public boolean compararTarjetas(Tarjeta cliente1, Tarjeta cliente2) {

		if (cliente1.getNumeroTarjeta().compareTo(cliente2.getNumeroTarjeta()) == 0) {

			return true;
		}

		return false;
	}

}
