package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import prueba.MetodosAuxconTarjetas;
import tarjetas.Tarjeta;
import tarjetas.TarjetaAmex;
import tarjetas.TarjetaVisa;

class MetodosAuxConTarjetasTest {

	MetodosAuxconTarjetas aux = new MetodosAuxconTarjetas();

	Tarjeta cliente1 = new TarjetaAmex("12345679", "Giuliano", "Da Silva", "06/22");

	Tarjeta cliente2 = new TarjetaVisa("12345679", "Giuliano", "Da Silva", "06/22");

	Tarjeta cliente3 = new TarjetaVisa("456789123", "Giuliano", "Da Silva", "06/22");

	@Test
	void comprovarTarjetasIgualestest() {

		assertTrue(aux.compararTarjetas(cliente1, cliente2));
	}

	@Test
	void comprovarTarjetasDistintastest() {

		assertFalse(aux.compararTarjetas(cliente1, cliente3));
	}

}
