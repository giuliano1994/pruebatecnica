package tests;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

import prueba.ModuloProcesarTarjeta;
import tarjetas.Tarjeta;
import tarjetas.TarjetaNara;

public class ModuloProcesarTrajetaTest {
	// Tarjeta Happy
		Tarjeta tarjetaHappy = new TarjetaNara("123456789", "Giuliano", "Da Silva", "10/10");

		// Modulo Happy
		ModuloProcesarTarjeta mod = new ModuloProcesarTarjeta(tarjetaHappy, 200.0);

		// Modulo con costo superior a lo permitido
		ModuloProcesarTarjeta moduloCostoSuperior = new ModuloProcesarTarjeta(tarjetaHappy, 1200.0);

		// Modulo con costo ilegal
		ModuloProcesarTarjeta moduloCostoilegal = new ModuloProcesarTarjeta(tarjetaHappy, -1200.0);

		// Happy-path

		
		
		@Test
		public void operacionValidaTest() {
			boolean resultado = mod.GetOperacionValida();

			assertTrue(resultado);
		}

		// Test para saber si la tarjeta es valida
		@Test
		public void targetaValidaParaOperarTest() {

			boolean resultado = mod.GetTarjetaValidaParaOperar();

			assertTrue(resultado);

		}

		// Test para compras superiores a 1000
		@Test
		public void operacionNoValidaTest() {

			assertFalse(moduloCostoSuperior.GetOperacionValida());

		}

		// Test para calcular la tasa de una compra de 200
		@Test
		public void CalcularTasaInteresConNaranjaTest() {

			Double resultado = mod.GetTasaDeInteres();

			Double resultadoesperado = 200 + (200 * ((mod.obtenerMesInt() * 0.5) / 100));

			assertEquals(resultadoesperado, resultado);

		}

		// Test para compras con valores <= 0
		// Debe saltar exepcion ya que se ingreso un numero negativo o 0

		@Test (expected = Exception.class)
		public void operacionInvalidaTest() {
			
			 moduloCostoilegal.GetOperacionValida();
			
		}
}
