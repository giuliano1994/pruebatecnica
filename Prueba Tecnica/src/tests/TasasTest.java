package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import prueba.Tasas;

class TasasTest {

	Tasas tasa = new Tasas();

	// Test para calcular tasa de tarjeta visa
	@Test
	public void getTasaTarjetaVisaTest() {
		Double resultado = tasa.getTasaVisa(5, 10, (double) 500);

		assertEquals(750, resultado);
	}

	// Test para calcular tasa de tarjeta Naranja
	@Test
	public void getTasaTarjetaNaraTest() {
		Double resultado = tasa.getTasaNara(5, (double) 500);

		assertEquals(512.5, resultado);
	}

	// Test para calcular tasa de tarjeta American Express
	@Test
	public void getTasaTarjetaAmexTest() {
		Double resultado = tasa.getTasaAmex(3, (double) 500);

		assertEquals(650, resultado);

	}

}
