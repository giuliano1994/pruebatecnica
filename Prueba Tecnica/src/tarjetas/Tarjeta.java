//Clase abstracta para modelar tarjetas

package tarjetas;

import java.io.Serializable;

public abstract class Tarjeta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String numeroTarjeta;
	protected String nombreTitular;
	protected String apellidoTitular;
	protected String fechaVencimiento;

	// Constructor
	public Tarjeta(String numeroTarjeta, String nombreTitular, String apellidoTitular, String fechaVencimiento) {

		this.numeroTarjeta = numeroTarjeta;
		this.nombreTitular = nombreTitular;
		this.apellidoTitular = apellidoTitular;
		this.fechaVencimiento = fechaVencimiento;

	}

	// Metodos abstractos, son los que deben cumplir las clases que hereden de esta
	public abstract String getMarca();

	public abstract String getNumeroTarjeta();

	public abstract String getNombreTitular();

	public abstract String getApellidoTitular();

	public abstract String getFechaVencimiento();

	public abstract boolean compareTo(Tarjeta cliente2);

	@Override
	public String toString() {
		return "Tarjeta [numeroTarjeta=" + numeroTarjeta + ", nombreTitular=" + nombreTitular + ", apellidoTitular="
				+ apellidoTitular + ", fechaVencimiento=" + fechaVencimiento + "]";
	}

}
