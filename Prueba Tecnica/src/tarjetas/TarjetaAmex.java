package tarjetas;

public class TarjetaAmex extends Tarjeta {

	private String marca;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Constructor
	public TarjetaAmex(String numeroTarjeta, String nombreTitular, String apellidoTitular, String fechaVencimiento) {
		super(numeroTarjeta, nombreTitular, apellidoTitular, fechaVencimiento);

		marca = "Amex";

	}

	// Metodos

	public String getMarca() {
		return marca;
	}

	@Override
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	@Override
	public String getNombreTitular() {
		return nombreTitular;
	}

	@Override
	public String getApellidoTitular() {
		return apellidoTitular;
	}

	@Override
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	@Override
	public boolean compareTo(Tarjeta cliente2) {
		if (numeroTarjeta.compareTo(cliente2.getNumeroTarjeta()) == 0) {

			return true;
		}
		return false;
	}

}
